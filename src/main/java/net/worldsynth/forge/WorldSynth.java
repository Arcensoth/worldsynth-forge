/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge;

import java.io.File;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.worldsynth.forge.proxy.CommonProxy;
import net.worldsynth.forge.synth.InvalidSynthException;
import net.worldsynth.forge.synth.SynthWrapper;
import net.worldsynth.forge.world.WorldTypeWorldSynth;
import net.worldsynth.synth.Synth;
import net.worldsynth.synth.io.ProjectReader;

@Mod(modid = WorldSynth.MOD_ID, acceptedMinecraftVersions = "[1.12.2]", useMetadata = true)
public class WorldSynth {
	
	public static final String MOD_ID = "worldsynthforge";
	
	public static final Logger LOGGER = LogManager.getLogger("worldsynthforge");

	@Instance
	public static WorldSynth instance;

	@SidedProxy(clientSide = "com.booleanbyte.worldsynth.forge.proxy.ClientProxy", serverSide = "com.booleanbyte.worldsynth.forge.proxy.CommonProxy")
	public static CommonProxy proxy;

	@EventHandler
	public static void preInit(FMLPreInitializationEvent event) {
		proxy.preInit(event);
		
		LOGGER.info("Preparing to import worldsynths");
		File synthsDir = new File(Loader.instance().getConfigDir().getParentFile(), "worldsynth/synths/");
		if(synthsDir.isDirectory()) {
			for(SynthWrapper s: getSynths(synthsDir)) {
				new WorldTypeWorldSynth(s);
			}
		}
	}
	
	private static ArrayList<SynthWrapper> getSynths(File synthsDir) {
		ArrayList<SynthWrapper> synths = new ArrayList<SynthWrapper>();
		
		for(File f: synthsDir.listFiles()) {
			if(!f.getName().endsWith(".wsynth")) {
				continue;
			}
			
			Synth s = ProjectReader.readSynthFromFile(f);
			try {
				LOGGER.info("Importing \"" + s.getName() + "\"");
				synths.add(new SynthWrapper(s));
			} catch (InvalidSynthException e) {
				LOGGER.info("Importing \"" + s.getName() + "\" faild as it is not valid a synth for worldsynth forge");
				e.printStackTrace();
			}
		}
		
		return synths;
	}
}
