/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.proxy;

import java.io.File;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.worldsynth.addon.minecraft.module.forge.ModuleMinecraftForgeExport;
import net.worldsynth.common.WorldSynthCore;
import net.worldsynth.forge.WorldSynth;
import net.worldsynth.module.ClassNotModuleExeption;

public class CommonProxy {

	public void preInit(FMLPreInitializationEvent event) {
		WorldSynth.LOGGER.info("Initializing WorldSynth Engine");
		
		//Get the root directory of the minecraft instance
		File mcDir = Loader.instance().getConfigDir().getParentFile();
		
		//Get the worldsynth directory for worldsynth configs and other 
		File worldSynthDir = new File(mcDir, "worldsynth");
		if(mcDir.exists() && !worldSynthDir.exists()) {
			worldSynthDir.mkdir();
		}
		
		//Get the worldsynth directory for worldsynth configs and other 
		File synthDir = new File(worldSynthDir, "synths");
		if(worldSynthDir.exists() && !synthDir.exists()) {
			synthDir.mkdir();
		}
		
		//Initialize the worldsynth engine
		new WorldSynthCore(worldSynthDir);
		try {
			WorldSynthCore.moduleRegister.registerModule(ModuleMinecraftForgeExport.class, "\\Minecraft");
		} catch (ClassNotModuleExeption e) {
			e.printStackTrace();
		}
		
		WorldSynth.LOGGER.info("Done initializing WorldSynth Engine");
	}
}
