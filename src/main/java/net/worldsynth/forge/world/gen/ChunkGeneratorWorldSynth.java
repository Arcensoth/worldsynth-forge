/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.world.gen;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.SpawnListEntry;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkPrimer;
import net.minecraft.world.gen.IChunkGenerator;
import net.worldsynth.forge.synth.SynthWrapper;
import net.worldsynth.material.MaterialRegistry;

public class ChunkGeneratorWorldSynth implements IChunkGenerator {

	private final World world;
	private final SynthWrapper generatorSynth;
	private final long seed;

	public ChunkGeneratorWorldSynth(World worldIn, long seed, SynthWrapper synth, String generatorOptions) {
		this.world = worldIn;
		this.seed = seed;
		this.generatorSynth = synth;
	}

	@Override
	public Chunk generateChunk(int x, int z) {
		int[][][] chunkBlockspace = generatorSynth.getChunkBlockspace(x, z).blockspaceMaterialId;
		ChunkPrimer chunkprimer = new ChunkPrimer();

		for (int v = 0; v < 256; ++v) {
			for (int u = 0; u < 16; ++u) {
				for (int w = 0; w < 16; ++w) {
					IBlockState b = Block.getStateById(MaterialRegistry.getMaterialByInternalId(chunkBlockspace[u][v][w]).getId());
					chunkprimer.setBlockState(u, v, w, b);
				}
			}
		}

		Chunk chunk = new Chunk(this.world, chunkprimer, x, z);
		Biome[] abiome = this.world.getBiomeProvider().getBiomes((Biome[]) null, x * 16, z * 16, 16, 16);
		byte[] abyte = chunk.getBiomeArray();

		for (int l = 0; l < abyte.length; ++l) {
			abyte[l] = (byte) Biome.getIdForBiome(abiome[l]);
		}

		chunk.generateSkylightMap();
		return chunk;
	}

	@Override
	public void populate(int x, int z) {

	}

	@Override
	public boolean generateStructures(Chunk chunkIn, int x, int z) {
		return false;
	}

	@Override
	public List<SpawnListEntry> getPossibleCreatures(EnumCreatureType creatureType, BlockPos pos) {
		return new ArrayList<Biome.SpawnListEntry>();
	}

	@Override
	public BlockPos getNearestStructurePos(World worldIn, String structureName, BlockPos position, boolean findUnexplored) {
		return new BlockPos(0, 0, 0);
	}

	@Override
	public void recreateStructures(Chunk chunkIn, int x, int z) {

	}

	@Override
	public boolean isInsideStructure(World worldIn, String structureName, BlockPos pos) {
		return false;
	}

}
