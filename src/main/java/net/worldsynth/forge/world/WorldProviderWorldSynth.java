/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.world;

import net.minecraft.world.DimensionType;
import net.minecraft.world.WorldProvider;

public class WorldProviderWorldSynth extends WorldProvider {
	private final String name = "WorldSynth";
	private final String suffix = "";
	private final int id = 1000;
	private final Class <? extends WorldProvider> provider = WorldProviderWorldSynth.class;
	private final boolean keepLoaded = false;
	
	private final DimensionType dimensionType = DimensionType.register(name, suffix, id, provider, keepLoaded);
	
	@Override
	public DimensionType getDimensionType() {
		return dimensionType;
	}

}
