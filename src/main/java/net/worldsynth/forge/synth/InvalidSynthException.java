/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.forge.synth;

import net.worldsynth.synth.Synth;

public class InvalidSynthException extends Exception {
	private static final long serialVersionUID = -6667918578298664070L;
	
	private final Synth synth;
	
	InvalidSynthException(String message, Synth synth) {
		super(message);
		this.synth = synth;
	}
	
	public Synth getSynth() {
		return synth;
	}
}
